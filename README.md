AIRA
====

Unified CLI for running foreground tasks. Tasks for AIRA can be provided by installing npm module from `@braira` namespace. These modules will be detected automatically and you can use them just right after the installation.

CLI Example
-----------

_installation_

```shell
npm install @bracketedrebels/aira -g
npm install @braira/notify -g
```
_usage_

```shell
aira --help
aira notify --help
```

CI/CD Example
-------------

_.gitlab-ci.yml_
```yaml
image: node:8.9.1

notifying:
  before_script:
    - npm install @bracketedrebels/aira -g
    - npm install @braira/notify -g
  only:
    - tags
  script:
    - aira notify $SLACK_WEBHOOK .notification.slack.json --payload.version=$(git describe --tags)
```

See [packages in `@braira` namespace](https://www.npmjs.com/org/braira) for list of accessable modules for AIRA to run
