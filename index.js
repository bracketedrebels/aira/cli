#!/usr/bin/env node

// it is assumed, that name of the CLI will be also
// NPM org, and all packages under it will be YARGS
// commands.

try {
  const fs = require('fs');
  const path = require('path');
  const process = require('process');
  const yargs = require('yargs');
  const debug = require('debug')(require('./package.json').name);

  const contextDir = path.dirname(fs.realpathSync(process.argv[1]));
  const commandsDir = fs.existsSync(path.resolve(contextDir, '../../@braira'))
    ? path.resolve(contextDir, '../../@braira')
    : fs.existsSync(path.resolve(contextDir, 'node_modules/@braira'))
      ? path.resolve(contextDir, 'node_modules/@braira')
      : process.cwd();

  debug('Available commands:');
  fs.readdirSync(commandsDir)
    .map(v => (debug(v), v))
    .map(v => path.resolve(commandsDir, v))
    .filter(v => fs.existsSync(path.resolve(v, 'package.json')))
    .map(v => require(v))
    .reduce((acc, cmd) => (acc.command(cmd), acc), yargs)
    .demandCommand()
    .help()
    .argv
} catch (e) {
  console.error(e);
  process.exit(1);
}
